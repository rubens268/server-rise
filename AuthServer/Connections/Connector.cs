﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Collections.Generic;
using System.Reflection;
using SimpleJSON;

namespace AuthServer.Connections
{
    class Connector
    {
        #region DECLARACION DE VARIABLES
        /**
         * Puntero a TCPListener
         *
         * @access private
         * @var TcpListener
         */
        static private TcpListener serverSocket;

        /**
         * Puntero a clientSocket
         *
         * @access private
         * @var clientSocket
         */
        static private TcpClient clientSocket;
        

        /**
         * Contador de conexiones (Sirve como identificador unico de red)
         *
         * @access private
         * @var int
         */
        static public int clientCounter = 0;
        
        /**
         * Function startServer
         *
         * Abre el servidor en el puerto indicado
         *
         * @access public
         * @return void
         */
        #endregion

        #region AUTHSERVER
        public static void startServer()
        {
            serverSocket = new TcpListener(IPAddress.Any, Config.instance.port);
            serverSocket.Start();

            clientSocket = default(TcpClient);
            clientCounter = 0;

            while (true)
            {
                clientSocket = serverSocket.AcceptTcpClient();

                //al entrar un cliente
                clientCounter++;
                Write.write("New connection, ip = " + ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address + " ID = " + clientCounter + ".");
                User client = new User();
                
                Config.instance.clients.Add(client.startClient(clientSocket, clientCounter, ((IPEndPoint)clientSocket.Client.RemoteEndPoint).Address));
            }
        }
        #endregion
    }
    
}

