﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthServer
{
    class Write
    {
        public static void write (string msg)
        {
            string Minutes = DateTime.Now.Minute.ToString();
            if (DateTime.Now.Minute.ToString().Length == 1)
            {
                Minutes = "0" + DateTime.Now.Minute.ToString();
            }
            string Seconds = DateTime.Now.Second.ToString();
            if (DateTime.Now.Second.ToString().Length == 1)
            {
                Seconds = "0" + DateTime.Now.Second.ToString();
            }

            Console.WriteLine("[" + DateTime.Now.Hour.ToString() + ":" + Minutes + ":" + Seconds + "]" + msg);
        }
    }
}
