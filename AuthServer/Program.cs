﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using AuthServer.Threads;
using BCrypt.Net;
using MySql.Data.MySqlClient;
using AuthServer.Connections;
using AuthServer.DB;

namespace AuthServer
{
    class Program
    { 
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al servidor Auth de Articon Entertainment");
            
            
            
            reask:
            Console.WriteLine("Usuario:");
            string username = Console.ReadLine();
            Console.WriteLine("Contraseña:");
            string password = ReadPassword();
            if (db.instance.Login(username, password, 2))
            {
                Console.WriteLine("Bienvenido " + Config.instance.username + ".");
                ListeningThread.instance.startThread();
                ConsoleThread.instance.startThread();
                Thread mainServer = new Thread(new ThreadStart(Connector.startServer));
                mainServer.Start();
            }
            else
            {
                Console.WriteLine(db.instance.LoginEx);
                goto reask;
            }
            
        }

        

        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        // remove one character from the list of password characters
                        password = password.Substring(0, password.Length - 1);
                        // get the location of the cursor
                        int pos = Console.CursorLeft;
                        // move the cursor to the left by one character
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        // replace it with space
                        Console.Write(" ");
                        // move the cursor to the left by one character again
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            // add a new line because user pressed enter at the end of their password
            Console.WriteLine();
            return password;
        }
    }
}
