﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace AuthServer.Connections
{
    class ServersCertificate
    {
        public static ServersCertificate instance = new ServersCertificate();
        public X509Certificate getServerCert()
        {
            byte[] c = Certificate.CreateSelfSignCertificatePfx(
        "CN=articonentertainment.com", //host name
        DateTime.Parse("2015-01-01"), //not valid before
        DateTime.Parse("2016-12-01"), //not valid after
        "articon32"); //password to encrypt key file
            using (BinaryWriter binWriter = new BinaryWriter(
                File.Open(@"testcert.pfx", FileMode.Create)))
            {
                binWriter.Write(c);
            }
            X509Certificate cert = new X509Certificate2(@"testcert.pfx", "articon32");

            return cert;
        }
    }
}
