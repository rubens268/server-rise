﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Collections;

namespace AuthServer.DB
{
    class db
    {
        public static db instance = new db();
        public MySqlConnection database;
        public string LoginEx;

        #region connect/disconnect
        public void Connect()
        {
            string MySQLconnectionString = "SERVER=" + Config.instance.SQLServer + ";" + "DATABASE=" + Config.instance.SQLDatabase + ";" + "UID=" + Config.instance.SQLUser + ";" + "PASSWORD=" + Config.instance.SQLPass + ";";
            try
            {

                database = new MySqlConnection(MySQLconnectionString);
                database.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void Disconnect()
        {
            database.Close();
        }
        #endregion

        #region getUserID
        public int getUserID(string UserName)
        {
            
            int id = -2;
            string query = "SELECT * FROM Users WHERE UserName='" + UserName + "'";
            MySqlDataReader dataReader = null;
            MySqlCommand cmd = new MySqlCommand(query, db.instance.database);
            try
            {
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    id = Convert.ToInt32(dataReader["ID"]);
                }
                dataReader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }
            return id;
        }
        #endregion

        #region Login
        public bool Login(string username, string password, int neededRange)
        {
            Connect();
            string hash = "";
            string range = "";

            string query = "SELECT * FROM Users WHERE UserName='" + username + "'";
            MySqlDataReader dataReader = null;
            MySqlCommand cmd = new MySqlCommand(query, db.instance.database);
            try
            {
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    hash = dataReader["PassWd"] + "";
                    range = dataReader["Perms"] + "";
                }
                dataReader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }


            Disconnect();


            if (hash == "")
            {
                LoginEx = "Usuario o Contraseña incorrectos.";
                return false;
            }


            if (Convert.ToInt32(range) < neededRange)
            {
                LoginEx = "Insuficiente rango.";
                return false;
            }


            if (BCrypt.Net.BCrypt.Verify(password, hash))
            {
                Config.instance.username = username;
                return true;
            }
            else
            {
                LoginEx = "Usuario o Contraseña incorrectos.";
                return false;
            }
        }
        #endregion

        #region AddUser
        public void AddUser (string UserName, string PassWd)
        {
            Connect();
            string query = "SELECT * FROM Users WHERE UserName='" + UserName + "'";
            MySqlDataReader dataReader = null;
            MySqlCommand cmd = new MySqlCommand(query, database);
            try
            {
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    if (dataReader["UserName"].ToString() == UserName)
                    {
                        throw new Exception("El usuario ya existe!");
                    }
                }
                dataReader.Close();
                PassWd = BCrypt.Net.BCrypt.HashString(PassWd);
                query = "INSERT INTO Users (UserName, PassWd, Perms)VALUES('" + UserName + "', '" + PassWd + "', '1')";
                cmd = new MySqlCommand(query, database);
                cmd.ExecuteNonQuery();
                dataReader.Close();
            }
            catch (MySqlException ex)
            {
                Write.write(ex.ToString());
            }
            catch (Exception ex)
            {
                Write.write(ex.ToString());
            }
            Disconnect();
        }
        #endregion

        #region addFriend
        public void addFriend (string UserName1, string UserName2)
        {
            Connect();
            string query = "INSERT INTO Friends (IDUser1,IDUser2) VALUES("+ getUserID(UserName1) + ","+ getUserID(UserName2) + ")";
            
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, database);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Write.write(e.ToString());
            }
            Disconnect();
        }

        #endregion

        #region getFriends

        public void getFriends (string UserName)
        {
            Connect();
            int userID = getUserID(UserName);
            string query = "SELECT * FROM Friends WHERE IDUser1='" + userID + "'";
            List <int> friends = new List<int>();
            int i = 0;
            MySqlDataReader dataReader = null;
            ArrayList al = new ArrayList();
            MySqlCommand cmd = new MySqlCommand(query, db.instance.database);
            try
            {
                dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    friends.Add(Convert.ToInt32(dataReader["IDUser2"]));
                }
                dataReader.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex);
            }

            
            Disconnect();

            foreach (int friend in friends)
            {
                Write.write(friend.ToString());
            }
        }

        #endregion

        #region SetAdmin
        public void SetAdmin (string UserName, string AdminLevel)
        {
            Connect();
            string query = "UPDATE Users SET Perms = '" + AdminLevel + "' WHERE UserName = '" + UserName + "'";
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, database);
                cmd.ExecuteNonQuery();
            }
            catch(MySqlException e)
            {
                Write.write(e.ToString());
            }
            Disconnect();
        }
        #endregion

        #region BanUser
        public void BanUser (string UserName)
        {
            Connect();
            string query = "UPDATE Users SET Enabled = 'false' WHERE UserName = '" + UserName + "'";
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, database);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Write.write(e.ToString());
            }
            Disconnect();
        }
        #endregion

        #region unBanUser
        public void unBanUser(string UserName)
        {
            Connect();
            string query = "UPDATE Users SET Enabled = 'true' WHERE UserName = '" + UserName + "'";
            try
            {
                MySqlCommand cmd = new MySqlCommand(query, database);
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException e)
            {
                Write.write(e.ToString());
            }
            Disconnect();
        }
        #endregion

        
    }
}
