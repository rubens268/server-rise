﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Text.RegularExpressions;
using AuthServer.DB;
using AuthServer.Connections;

namespace AuthServer.Threads
{
    class ConsoleThread
    {
        public static ConsoleThread instance = new ConsoleThread();
        public Thread ConsoleThreaded;

        public void closeThread()
        {
            ConsoleThreaded.Abort();
        }

        public void startThread()
        {
            Write.write("Inciando Console Thread.");
            ConsoleThreaded = new Thread(readConsole);
            ConsoleThreaded.Start();
        }

        public void readConsole()
        {
            for (;;)
            {
                Console.Write(">>");
                string msg = Console.ReadLine();
                processMsg(msg);
            }
        }

        public void processMsg(string mess)
        {
            string[] msg = mess.Split(' ');
            switch (msg[0])
            {
                case "about":
                    Write.write("Launcher creado por ruben@articonentertainment.com");
                    break;
                case "clear":
                    Console.Clear();
                    break;
                case "exit":
                Environment.Exit(0);
                    break;
                case "adduser":
                    if (msg.Length == 3)
                    {
                        db.instance.AddUser(msg[1], msg[2]);
                    }
                    else if (msg.Length == 2)
                    {
                        Write.write("Falta la contraseña, el comando se usa: adduser usuario contraseña");
                    }
                    else if (msg.Length == 1)
                    {
                        Write.write("Falta el usuario, el comando se usa: adduser usuario contraseña");
                    }
                    break;
                case "setadmin":
                    if (msg.Length == 3)
                    {
                        db.instance.SetAdmin(msg[1], msg[2]);
                    }
                    else if (msg.Length == 2)
                    {
                        Write.write("Falta el rango, el comando se usa: setadmin usuario rango");
                    }
                    else if (msg.Length == 1)
                    {
                        Write.write("Falta el usuario, el comando se usa: setadmin usuario rango");
                    }
                    break;
                case "kick":
                    if (msg.Length == 2)
                        foreach (User user in Config.instance.clients)
                        {
                            if (user.username == msg[1])
                            {
                                Messages.instance.kick(user);
                            }
                        }
                    else
                        Write.write("Falta el usuario, el comando se usa: kick usuario");
                    break;
                case "ban":
                    if (msg.Length == 2)
                        db.instance.BanUser(msg[1]);
                    else
                        Write.write("Falta el usuario, el comando se usa: kick usuario");
                    break;
                case "addfriend":
                    if (msg.Length == 3)
                    {
                        db.instance.addFriend(msg[1], msg[2]);
                    }
                    else if (msg.Length == 2)
                    {
                        Write.write("Falta el primer usuario, el comando se usa: addfirend usuario usuario");
                    }
                    else if (msg.Length == 1)
                    {
                        Write.write("Falta el segundo usuario, el comando se usa: addfirend usuario usuario");
                    }
                    break;
                case "getfriends":
                    if (msg.Length == 2)
                        db.instance.getFriends(msg[1]);
                    else
                        Write.write("Falta el usuario, el comando se usa: getfriends usuario");
                    break;

                ////////////////////////////////////
                // Comando help ultimo
                ////////////////////////////////////
                default:
                case "help":
                case "/":
                    List<string> help = new List<string>() {
                        "addfriend - addfriend usuario1 usuario2 - Añade el amigo usuario2 al usuario1",
                        "getfriends - getfriends usuario - Obtiene la lista de amigso del usuario",
                        "kick - kick usuario - Echa al usuario",
                        "setadmin - setadmin usuario ranago - Modifica el rango de un usuario",
                        "adduser - adduser usuario contraseña - Añade un usuario a la base de datos",
                        "about - Información de quien lo ha hecho",
                        "clear - Limpia la consola",
                        "exit - Cierra el programa",
                        "help o / - Este texto"
                    };

                    foreach (string str in help)
                    {
                        Write.write(str);
                    }
                    break;
        }
            

        }
    }
}