﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net.Security;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;
using System.Net;

namespace AuthServer.Connections
{
    class SSLTcpClient
    {
        public static SSLTcpClient instance = new SSLTcpClient();
        public TcpClient sslClient = new TcpClient();
        public SslStream sslStream;


        public bool Connected = false;
        public void Start() 
        {
            try
            {
                IAsyncResult result = sslClient.BeginConnect(Config.instance.serverIP, Config.instance.port, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(2));
                if (success)
                {
                    sslClient.EndConnect(result);
                    sslStream = new SslStream(sslClient.GetStream(), false, new RemoteCertificateValidationCallback(CertificateValidationCallBack), new LocalCertificateSelectionCallback(CertificateSelectionCallBack));
                    bool authenticationPassed = true;
                    try
                    {
                        string serverName = System.Environment.MachineName;
                        X509Certificate cert = ServersCertificate.instance.getServerCert();
                        X509CertificateCollection certs = new X509CertificateCollection();
                        certs.Add(cert);

                        sslStream.AuthenticateAsClient(serverName, certs, SslProtocols.Default, false);

                    }
                    catch (AuthenticationException e) { authenticationPassed = false; Console.WriteLine("[SSL Authentication]", "Authentication Exception: " + e.ToString()); }
                    if (authenticationPassed)
                    {
                        Connected = true;
                        Console.WriteLine("[SSL]", "Connected!");
                    }
                }
            }
            catch (SocketException socketError)
            {
                if (socketError.SocketErrorCode == SocketError.ConnectionRefused)
                {
                    Connected = false;
                    Console.WriteLine("[SSL SocketException]", socketError.SocketErrorCode.ToString());
                }
            }
            catch (TimeoutException) { Console.WriteLine("[SSL]", "Connection timeout!"); }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("[SSL]", "Exception: " + e.ToString());
            }
            catch (Exception ex) { Console.WriteLine("[SSL Start]", "Exception: " + ex.ToString()); };

        }


        private static bool CertificateValidationCallBack( object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {

            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                return true;
            }


            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                if (chain != null && chain.ChainStatus != null)
                {
                    foreach (X509ChainStatus status in chain.ChainStatus)
                    {
                        if ((certificate.Subject == certificate.Issuer) &&
                           (status.Status == X509ChainStatusFlags.UntrustedRoot))
                        {
                            continue;
                        }
                        else
                        {
                            if (status.Status != X509ChainStatusFlags.NoError)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        static X509Certificate CertificateSelectionCallBack(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            return localCertificates[0];
        }
    }
}
