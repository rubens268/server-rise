﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace AuthServer.Connections
{
    class Messages
    {
        public static Messages instance = new Messages();
        public void assignUserID (User client, int userID)
        {
            string AssignUserID = @"
                {
                            status: ok,
                            task: Login,
                            method: loginUser,
                            data: {
                                networkId: " + userID.ToString() + @"
                            }
                    }
                    ";
            client.Send(AssignUserID);
        }

        public void loginSuccesfull(User client)
        {
            string loginSuccesfull = @"
                {
                            status: ok,
                            task: Login,
                            method: status,
                            data: {
                                login: " + "ok" + @"
                            }
                    }
                    ";
            client.Send(loginSuccesfull);
        }

        public void kick(User client)
        {
            string AssignUserID = @"
                {
                            status: ok,
                            task: Disconnect,
                            method: Disconnect,
                    }
                    ";
            client.Send(AssignUserID);
        }
    }
}
