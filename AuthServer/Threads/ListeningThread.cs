﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using SimpleJSON;

namespace AuthServer.Threads
{
    class ListeningThread
    {
        public static ListeningThread instance = new ListeningThread();
        
        public void startThread()
        {
            Write.write("Escuchando...");
            
        }
        
        public void processMessage(JSONNode data, User client)
        {
            try
            {
                if (data["status"].Value.Length == 0) { Console.WriteLine("Mensaje en blanco"); }
                else
                {
                    if (data["task"].Value == "Login")
                    {
                        client.username = data["data"]["email"].Value;
                        client.password = data["data"]["password"].Value;
                        client.CheckPassword();
                    }
                    else { Console.WriteLine("[MessageUndefined]", "Message default: " + data["action"].Value.ToString()); }
                }
            }
            catch (Exception) { }
        }
    }
}
