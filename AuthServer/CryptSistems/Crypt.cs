﻿using System;
using System.Text;
using System.Security.Cryptography;


namespace AuthServer.Connections
{
    class Crypt
    {
        private static string key = "http://articonentertainment.com";
        public static string Encrypt(string txt)
        {
            byte[] keyArray;
            byte[] textToEncrypt = UTF8Encoding.UTF8.GetBytes(txt);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();

            byte[] textDecrypted = cTransform.TransformFinalBlock(textToEncrypt, 0, textToEncrypt.Length);

            tdes.Clear();

            return Convert.ToBase64String(textDecrypted, 0, textDecrypted.Length);
        }

        public static string Decrypt(string EncryptedText)
        {
            byte[] keyArray;
            byte[] textToDecrypt = Convert.FromBase64String(EncryptedText);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();

            byte[] DecryptedText = cTransform.TransformFinalBlock(textToDecrypt, 0, textToDecrypt.Length);

            tdes.Clear();

            return UTF8Encoding.UTF8.GetString(DecryptedText);
        }
    }
}
