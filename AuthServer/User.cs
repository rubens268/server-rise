﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using AuthServer.Connections;
using AuthServer.Threads;
using System.Net.Security;
using SimpleJSON;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;
using System.IO;


namespace AuthServer
{
    class User
    {
        public TcpClient client;
        public int ID;
        public string username;
        public string password;
        public IPAddress IP;
        public SslStream sslStream;

        public User startClient(TcpClient inClientSocket, int authNetworkID, IPAddress userIP)
        {
            this.IP = userIP;
            //Guardamos el clientsocket
            this.client = inClientSocket;

            //Guardamos a ID de red
            this.ID = authNetworkID;

            //Llamamos a la class Connections
            checkCertificate();

            Thread Treading = new Thread(ThreadFunctions);
            Treading.Start();

            Messages.instance.assignUserID(this, ID);
            return this;
        }
        public void ThreadFunctions()
        {
            for (;;)
            {
                ListeningThread.instance.processMessage(JSON.Parse(Read()), this);
            }
        }

        public void CheckPassword ()
        {
            if (DB.db.instance.Login(username, password, 0))
            {
                Write.write(username + " Logged in successfull");
                Messages.instance.loginSuccesfull(this);
            }
            else
            {
                Write.write(username + " tried to loggin");
            }
        }

        #region CERTIFICADOS
        public void checkCertificate()
        {
            try
            {
                sslStream = new SslStream(client.GetStream());
                X509Certificate cert = getServerCert();
                sslStream.AuthenticateAsServer(cert);
                
            }
            catch (AuthenticationException) { }
            catch (Exception) { }
        }

        private X509Certificate getServerCert()
        {
            byte[] c = Certificate.CreateSelfSignCertificatePfx(
            "CN=articonentertainment.com", //host name
            DateTime.Parse("2015-01-01"), //not valid before
            DateTime.Parse("2016-12-01"), //not valid after
            "articon32"); //password to encrypt key file

            using (BinaryWriter binWriter = new BinaryWriter(
                File.Open(@"CoRcer.pfx", FileMode.Create)))
            {
                binWriter.Write(c);
            }

            X509Certificate cert = new X509Certificate2(@"CoRcer.pfx", "articon32");

            return cert;
        }
        #endregion

        #region SEND - READ
        public void Send(string text)
        {
            Byte[] sendBytes = null;

            string encryptedJSON = Crypt.Encrypt(text) + "<EOM>";

            sendBytes = Encoding.UTF8.GetBytes(encryptedJSON);

            sslStream.Write(sendBytes, 0, sendBytes.Length);
            sslStream.Flush();
        }



        public string Read()
        {
            if (client != null)
            {
                byte[] bytesFrom = new byte[10025];

                sslStream.Read(bytesFrom, 0, bytesFrom.Length);

                //Pasamos de Bytes a Texto
                string dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom);
                
                //Limitamos la cadena de texto
                dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("<EOM>"));
                dataFromClient = Crypt.Decrypt(dataFromClient);

                return dataFromClient;
            }
            else
                return "null";

        }
        #endregion
    }
}
